window.onload = function() {
    let isLoggedIn = sessionStorage.getItem("isLoggedIn");
    let username = localStorage.getItem("username");
    let isUser = localStorage.getItem("isUser");
    let isAdmin = localStorage.getItem("isAdmin");

    if (isUser === "true") {
        alert("bạn không phải là admin ");
        window.location.href = "user.html";
    }

    if (isLoggedIn && username && isAdmin === "true") {
        document.getElementById("loginStatus").innerHTML = "Đã đăng nhập với tên đăng nhập: " + username;
    } else {
        window.location.href = "login.html";
    }
};