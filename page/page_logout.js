// Xóa thông tin đăng nhập khỏi Local Storage và Session Storage
function logout() {
    localStorage.removeItem("username");
    localStorage.removeItem("isAdmin");
    localStorage.removeItem("isUser");
    sessionStorage.removeItem("isLoggedIn");

    window.location.href = "login.html";
}
logout();