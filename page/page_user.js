
window.onload = function() {
    let isLoggedIn = sessionStorage.getItem("isLoggedIn");
    let username = localStorage.getItem("username");
    let isUser = localStorage.getItem("isUser");
    let isAdmin = localStorage.getItem("isAdmin");

    if (isLoggedIn && username && (isUser === "true" || isAdmin === "true")) {
        document.getElementById("loginStatus").innerHTML = "Đã đăng nhập với tên đăng nhập: " + username;
    } else {
        window.location.href = "login.html";
    }

};
