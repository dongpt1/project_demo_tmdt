// import { User } from "../js/entity";

const USERS_KEY = 'users_key';
// Lưu thông tin đăng nhập vào Local Storage
export function saveLoginInfo() {
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;
    let isAdmin = false;
    let isUser = false;

    // đoạn này anh đang gán cứng có 2 tài khoản user và admin để check
    if(username === "admin" && password === "admin"){
        isAdmin = true;
    }else if(username === "user" && password === "user"){
        isUser = true;
    }else{
        alert("tài khoản hoặc mật khẩu không chính xác, mời nhập lại");
        return;
    }

    localStorage.setItem("username", username);
    localStorage.setItem("isAdmin", isAdmin);
    localStorage.setItem("isUser", isUser);
    sessionStorage.setItem("isLoggedIn", true);

    if (isAdmin) {
        window.location.href = "admin.html";
    }
    if (isUser) {
        window.location.href = "user.html";
    }
}

function demo(){
    alert('xxx');
}
// tạo tài khoản role user
function saveRegisterInfo(){
    let username = document.getElementById("registerUsername").value;
    let password = document.getElementById("registerPassword").value;
    let repassword = document.getElementById("registerRepeatPassword").value;
    let checkHelpText = document.getElementById("registerCheckHelpText").value;

    let usersStorage = localStorage.getItem("users_key");

    let users;
    if(usersStorage == null){
        users = []
    }else{
        users = JSON.parse(usersStorage);
    }


    if (repassword !== password){
        alert("Mật khẩu không khớp, mời nhật lại");
        return;
    }

    if (checkHelpText == false){
        alert("Bạn cần đọc và đồng ý các điều khoản trước khi đăng ký");
        return;
    }

    let userNew = new User(username, password)
    // check username đã tồi tại
    for (const user of users) {
        if(user.getUsername === userNew.getUsername){
            alert(`Tên người dùng ${userNew.getUsername()} đã tồn tại`);
            return;
        }
    }

    // push user mới vào danh sách user trong local storage
    users.push(userNew);
    localStorage.setItem(USERS_KEY, JSON.stringify(users));

    // lưu dữ liệu mới vào storage
    localStorage.setItem("username", username);
    // nếu đã chuyển sang user có role thì dùng cách này
    // localStorage.setItem("role", userNew.getRole());
    localStorage.setItem("isUser", true);
    sessionStorage.setItem("isLoggedIn", true);
    // vào màn hình user
    window.location.href = "user.html";

}

// ban đầu lúc vào trang login, kiểm tra xem đã đăng nhập chưa  để chuyển trang
function init(){
    let isUser = localStorage.getItem("isUser");
    let isAdmin = localStorage.getItem("isAdmin");
    let isLoggedIn = sessionStorage.getItem("isLoggedIn");

    if (isLoggedIn && isAdmin) {
        window.location.href = "admin.html";
    }
    if (isLoggedIn && isUser) {
        window.location.href = "user.html";
    }
}
init();